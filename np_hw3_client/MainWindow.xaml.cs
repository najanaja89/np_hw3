﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Windows.Threading;

namespace np_hw3_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static NetworkStream stream;
        static TcpClient client;

        public MainWindow()
        {
            InitializeComponent();
            textBoxUserName.Text = $"{System.Environment.MachineName.ToString()}: ";
        }

        private void ButtonSendClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(SendMessage);
            //SendMessage();
        }


        private void ButtonConnectClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(ClientThreadRoutine);
        }

        void ClientThreadRoutine(object obj)
        {


            string host = "";
            Dispatcher.Invoke(() => { host = textBoxServerIp.Text; });
            int port = 12345;
            Dispatcher.Invoke(() => { port = int.Parse(textBoxServerPort.Text); });
            string userName = "";
            Dispatcher.Invoke(() => { userName = textBoxUserName.Text; });
            try
            {
                //NetworkStream stream;
                client = new TcpClient(host, port); //подключение клиента
                stream = client.GetStream(); // получаем поток

                string message = userName;
                byte[] data = Encoding.Unicode.GetBytes(message);
                stream.Write(data, 0, data.Length);

                // запускаем новый поток для получения данных

                ThreadPool.QueueUserWorkItem(ReceiveMessage);

                Dispatcher.Invoke(() => { textBoxMessages.AppendText($"Добро пожаловать, {userName}"); });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        /// <summary>
        /// /////////////////////////////////////////////////////////////////////////
        /// </summary>
        void SendMessage(object state)
        {
            //var stream = obj as NetworkStream;
            string message = "";
            Dispatcher.Invoke(() => { message = textBoxMessage.Text; });
            byte[] data = Encoding.Unicode.GetBytes(message);
            stream.Write(data, 0, data.Length);
            //Dispatcher.Invoke(() => { stream.Write(data, 0, data.Length); });

        }
        // получение сообщений
        void ReceiveMessage(object obj)
        {
            //var stream = obj as NetworkStream;
            while (true)
            {
                try
                {
                    byte[] data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                       //Dispatcher.Invoke(() => { bytes = stream.Read(data, 0, data.Length); });
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);

                    string message = builder.ToString();
                    Dispatcher.Invoke(() => { textBoxMessages.AppendText(message+"\n"); });
                }
                catch
                {
                    MessageBox.Show("Подключение прервано!"); //соединение было прервано
                    Disconnect();
                }
            }
        }

        private void ButtonDisconnetcClick(object sender, RoutedEventArgs e)
        {
            Disconnect();
        }

        static void Disconnect()
        {
            if (stream != null)
                stream.Close();//отключение потока
            if (client != null)
                client.Close();//отключение клиента
            Environment.Exit(0); //завершение процесса
        }

    }
}
